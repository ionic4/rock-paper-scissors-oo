import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private playOptions: string[];
  private playerChoice: string;
  private computerChoice: string;
  private result: string;
  private playerWins = 0;
  private computerWins = 0;

  constructor() { 
    this.playOptions = ['Rock', 'Paper', 'Scissors'];
  }

  public play(playerChoice: string): void {
    this.playerChoice = playerChoice;
    this.computerPlays();
    this.result = this.whoWinns();
  }

  public getPlayOptions(): Array<string> {
    return this.playOptions;
  }

  public getComputer(): string {
    return this.computerChoice;
  }

  public getResult(): string {
    return this.result;
  }

  public getPlayerWinns(): number {
    return this.playerWins;
  }

  public getComputerWinns(): number {
    return this.computerWins;
  }

  private computerPlays() {
    const rand = Math.floor(Math.random() * this.playOptions.length);
    this.computerChoice = this.playOptions[rand];
  }

  private whoWinns() {
    if (this.computerChoice === this.playerChoice) {
      return "It's a draw";
    } else if ((this.computerChoice === this.playOptions[0] && this.playerChoice === this.playOptions[1]) ||
      (this.computerChoice === this.playOptions[1] && this.playerChoice === this.playOptions[2]) ||
      (this.computerChoice === this.playOptions[2] && this.playerChoice === this.playOptions[0])) {
        this.playerWins++;
        return 'You win!';
    } else {
      this.computerWins++;
      return 'I win!';
    }
  }
}
